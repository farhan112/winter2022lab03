public class Food{
	
	public String nameOfFood;
	public double cost;
	public String cuisineType;
	
	public void rate(){
		
		System.out.println(this.nameOfFood + " is a 5 star dish for only " + this.cost + "$!");
		
	}
	
}