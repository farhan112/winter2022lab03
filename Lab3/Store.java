import java.util.Scanner;
public class Store{
	
	public static void main(String[] args){
		
		Scanner reader = new Scanner(System.in);
		Food[] menu = new Food[4];
		
		for(int i = 0; i < 4; i++){
			
			menu[i] = new Food();
			
			System.out.println("Enter the name of dish #" + (i+1) +".");
			menu[i].nameOfFood = reader.nextLine();
			System.out.println("Enter the cost of dish #" + (i+1) +".");
			menu[i].cost = Double.parseDouble(reader.nextLine());
			System.out.println("Enter the type of cuisine of dish #" + (i+1) +".");
			menu[i].cuisineType = reader.nextLine();
			
		}
		
		System.out.println("The name of dish #4 is: " + menu[3].nameOfFood);
		System.out.println("The cost of dish #4 is: " + menu[3].cost + "$");
		System.out.println("The type of cuisine of dish #4 is: " + menu[3].cuisineType);
		menu[3].rate();
		
	}
	
}