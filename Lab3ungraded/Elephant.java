public class Elephant{
	
	public int lifespan;
	public String scientificName; 
	public String habitat;
	
	public void trumpet(){
		
		System.out.println("The elephant toots from its " + habitat + " habitat.");
		
	}
	
	public void maintainEcosystem(){
		
		System.out.println("This elephant will maintain the ecosystem for the next " + lifespan + " years!");
		
	}
	
}