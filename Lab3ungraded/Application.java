public class Application{
	
	public static void main (String[] args){
		
		Elephant asianElephant = new Elephant();
		Elephant africanElephant = new Elephant();
		
		asianElephant.lifespan = 48;
		asianElephant.scientificName = "Elephas Maximus";
		asianElephant.habitat = "tropical";
		
		africanElephant.lifespan = 60;
		africanElephant.scientificName = "Loxodonta africana";
		africanElephant.habitat = "subtropical";
		
		asianElephant.trumpet();
		asianElephant.maintainEcosystem();
		
		africanElephant.trumpet();
		africanElephant.maintainEcosystem();
		
		Elephant[] herd = new Elephant[3];
		herd[0] = asianElephant;
		herd[1] = africanElephant;
		
		herd[2] = new Elephant();
		herd[2].lifespan = 60;
		herd[2].scientificName = "Elephas maximus borneensis";
		herd[2].habitat = "bornean tropical";
		
		System.out.println(herd[2].lifespan);
		System.out.println(herd[2].scientificName);
		System.out.println(herd[2].habitat);
	}
}